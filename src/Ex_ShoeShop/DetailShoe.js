import React, { Component } from "react";

export default class DetailShoe extends Component {
  render() {
    return (
      <div className="row pt-5 modal" id="myModal">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h1 className="modal-title" style={{ color: "red" }}>
                PRODUCT INFORMATION
              </h1>
              <button type="button" className="close" data-dismiss="modal">
                &times;
              </button>
            </div>
            <div className="modal-body row">
              <img src={this.props.detail.image} className="col-4" alt="" />
              <div className="col-8 ">
                <p>Name : {this.props.detail.name}</p>
                <p>Price: {this.props.detail.price}</p>
                <p>Description : {this.props.detail.description}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
