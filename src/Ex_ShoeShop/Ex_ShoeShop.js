import React, { Component } from "react";
import Cart from "./Cart";
import ItemShoe from "./ItemShoe";
import { DataShoe } from "./DataShoe";
import ListShoe from "./ListShoe";
import DetailShoe from "./DetailShoe";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: DataShoe,
    detail: DataShoe[1],
    cart: [],
  };

  handleChangDetail = (value) => {
    this.setState({ detail: value });
  };

  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      let cartItem = { ...shoe, number: 1 };
      cloneCart.push(cartItem);
    } else {
      cloneCart[index].number++;
    }
    this.setState({
      cart: cloneCart,
    });
  };

  render() {
    return (
      <div className="container">
        <Cart cart={this.state.cart} />
        <ListShoe
          handleAddToCart={this.handleAddToCart}
          handleChangDetail={this.handleChangDetail}
          data={this.state.shoeArr}
        />
        <DetailShoe detail={this.state.detail} />
      </div>
    );
  }
}
